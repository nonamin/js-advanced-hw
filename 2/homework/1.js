  var button1 = document.querySelector('.showButton[data-tab="1"]');
  var button2 = document.querySelector('.showButton[data-tab="2"]');
  var button3 = document.querySelector('.showButton[data-tab="3"]');
  var tab1 = document.querySelector('.tab[data-tab="1"]');
  var tab2 = document.querySelector('.tab[data-tab="2"]');
  var tab3 = document.querySelector('.tab[data-tab="3"]');
  // var button = document.querySelectorAll('.showButton');
  var tab = document.querySelectorAll('.tab');
  var buttonContainer = document.getElementById('buttonContainer');
  var closeButton = document.createElement('button');
      closeButton.innerText = "Закрыть вкладки";
      closeButton.className = "showButton";
      buttonContainer.appendChild(closeButton);

  function showtab(item){
    item.classList.add('active');
  }

  button1.onclick = function(event){
    showtab(tab1);
  }

  button2.onclick = function(event){
    showtab(tab2);
  }

  button3.onclick = function(event){
    showtab(tab3);
  }

  closeButton.onclick = function(event){
    tab.forEach(function(item){
      item.classList.remove('active'); 
    });
  }

  
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
