
    const CommentProto = {
      avatar: 'https://media.mnn.com/assets/images/2018/07/cat_eating_fancy_ice_cream.jpg.1080x0_q100_crop-scale.jpg',
      addLike: function(){
          this.likes += 1;
      }
  }


  const CommentsArray = [];

  function Comment ( name, text, avatar){
      this.name = name;
      this.text = text;
      if( avatar !== undefined){
          this.avatar = avatar;
      }
      this.likes = 0;

      Object.setPrototypeOf( this, CommentProto);
      CommentsArray.push( this );
  }


  function renderComments( arrayOfComments ) {
      const root = document.getElementById('root');
      arrayOfComments.forEach( function( comment ){

          let CommentNode = document.createElement('div');
          
          let title = document.createElement('h3');
              title.innerText = comment.name;

          let likes = document.createElement('span');
              likes.className = '_likes';
              likes.innerText = '(likes ' + comment.likes + ')';

              let addLike = comment.addLike.bind(comment);

              likes.addEventListener('click', function(){
                  addLike();
                  this.innerText = '(likes ' + comment.likes + ')';
              })

          let text = document.createElement('p');
              text.innerText = comment.text;

          let avatar = new Image(100, 100);
              avatar.src = comment.avatar;
              
              CommentNode.appendChild(title);
              CommentNode.appendChild(likes);
              CommentNode.appendChild(text);
              CommentNode.appendChild(avatar);

              root.appendChild(CommentNode);
      });
  }

document.addEventListener('DOMContentLoaded', function(){

  let myComment1 = new Comment('Mr. A', 'Hi');
  let myComment2 = new Comment('Mr. B', 'Ho');
  new Comment('Mr. C', 'Hey');
  new Comment('Mr. D', 'Hello', 'https://ichef.bbci.co.uk/images/ic/720x405/p0517py6.jpg');

  renderComments( CommentsArray );

});

/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    let myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    let CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/