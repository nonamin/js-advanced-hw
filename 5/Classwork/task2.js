  let colors = {
    background: 'purple',
    color: 'white'
  }

  let text = document.createElement('h1');
    document.body.appendChild(text);
    text.innerText = "I know how binding works in JS";
    
  function myCall( color ){
    document.body.style.background = this.background;
    document.body.style.color = color;
  }

  function myBind(){
    document.body.style.background = this.background;
    document.body.style.color = this.color;
  }

  function myApply( phrase ){
    document.body.style.background = this.background;
    document.body.style.color = this.color;
    text.innerText = phrase;
  }

  let phrase = ["I know how binding works in JS!"];
  myCall.call(colors, 'green');
  b = myBind.bind(colors);
  b();
  myApply.apply(colors, phrase);
/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
 

  // fucntion myCall( color ){
  //   document.body.style.background = this.background;
  //   document.body.style.color = color;
  // }
  // myCall.call( colors, 'red' );
