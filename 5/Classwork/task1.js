    let Train = {
        name:"Happy Train",
        speed: 40,
        passengers: 1,

        go: function() {
            console.log('Поезд "' + this.name + '"' + ' везёт ' + 'пассажиров ' + this.passengers + ' со скоростью ' + this.speed);
        },

        stop: function(){
            this.speed = 0;
            console.log('Поезд "' + this.name + '" остановился. ' +  'Скорость: ' + this.speed);
        },

        addPassenger: function(x){
            this.passengers += x;
            console.log('Поезд "' + this.name + '"' + ' подобрал ' + this.passengers + ' пассажиров ' );
        }
    };

    Train.go();
    Train.stop();
    Train.addPassenger(2);
 
/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
