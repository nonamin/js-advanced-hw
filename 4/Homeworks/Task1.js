    let myValidateForm = document.getElementById('myValidateForm');
    let username = document.getElementById('username');
    let email = document.getElementById('email');
    let pass = document.getElementById('password');
    let appleNumber = document.getElementById('appleNumber');
    let thank = document.getElementById('thank');
    let submitButton = document.getElementById('submit');
    let validateButton = document.getElementById('validateButton');
    let agree = document.getElementById('agree');
    
    function checkValidity(element, textError){
        element.validity.valid == true ? element.className = 'valid' : showError(element, textError);
    }

    function showError(element, textError){
        element.setCustomValidity(textError);
        element.className = 'invalid error';
    }

    function validateButtonClick(){
        checkValidity(username, "Как тебя зовут дружище?!");
        checkValidity(email, "Ну и зря, не получишь бандероль с яблоками!");
        checkValidity(pass,"Я никому не скажу наш секрет");
        
        appleNumber.value === 0 ? showError(appleNumber,"Ну хоть покушай немного... Яблочки вкусные") : appleNumber.className = 'valid';
        
    }

    validateButton.addEventListener('click', validateButtonClick);

    /*

        Документация:
        
        https://developer.mozilla.org/ru/docs/HTML/HTML5/Constraint_validation
        
        form.checkValidity() > Проверка всех полей формы на валидость
        form.reportValidity() > Проверяет все поля на валидность и выводит возле каждого из не прошедшего валидацию
            сообщение с ошибкой

        formElement.validity > Объект с параметрами валидности поля 
        formElement.setCustomValidity(message) > Метод который задаст validity.valid = false, и при попытке отправки
            сообщения выведет message в браузерный попал

        Классы для стилизации состояния элемента
        input:valid{}
        input:invalid{}

        
        Задание:
        
        Используя браузерное API для валидации форм реализовать валидацию следующей формы:
        

        - Имя пользователя: type:text -> validation: required; minlength = 2;  
            Если пустое выввести сообщение: "Как тебя зовут дружище?!"
        - Email: type: email -> validation: required; minlength = 3; validEmail;
            Если эмейл не валидный вывести сообщение "Ну и зря, не получишь бандероль с яблоками!"
        - Пароль: type: password -> validation: required; minlength = 8; maxlength=16;
            Если пустой вывести сообщение: "Я никому не скажу наш секрет";
        - Количество сьеденых яблок: type: number -> validation: required; minlength = 1; validNumber;
            Если количество 0 вывести эррор с сообщением "Ну хоть покушай немного... Яблочки вкусные"
        - Напиши спасибо за яблоки: type: text -> validation: required; 
            Если текст !== "спасибо" вывести эррор с сообщением "Фу, неблагодарный(-ая)!" используя setCustomValidity();

        - Согласен на обучение: type: checkbox -> validation: required;
            Если не выбран вывести эррор с сообщение: "Необразованные живут дольше! Хорошо подумай!"

        Внизу две кнопки:

        1) Обычный submit который отправит форму, если она валидна.
        2) Кнопка Validate(Проверить) которая запускает методы:
            - yourForm.checkValidity: и выводит на страницу сообщение с результатом проверки
            - yourForm.reportValidity: вызывает проверку всех правил и вывод сообщения с ошибкой, если такая есть

    */