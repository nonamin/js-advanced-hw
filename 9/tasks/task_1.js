/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomColor() {
    return 'rgb('+ getRandomIntInclusive(1,255) + ','+ getRandomIntInclusive(1,255) + ','+ getRandomIntInclusive(1,255) + ')';
  }

function colorBackground(){
    document.body.style.backgroundColor = getRandomColor();;
    localStorage.setItem('background', document.body.style.backgroundColor);
}

document.addEventListener('DOMContentLoaded', () => {
    
    const backgroundButton = document.getElementById('back');
    backgroundButton.addEventListener('click', colorBackground);
    document.body.style.backgroundColor = localStorage.getItem('background')
})
