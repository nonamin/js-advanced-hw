/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678


    */
   let Users = [];
   class User{
       constructor(login, pass){
           this.login = login;
           this.pass = pass;
           Users.push( this );
        } 
    }

    
    let localUsers = localStorage.getItem('users');
    localUsers !== null ?
        Users = JSON.parse(localUsers).map( (item) => new User( item.login, item.pass) ) : [];
    
    
    document.addEventListener('DOMContentLoaded', () =>{
        let login = document.getElementById('login');
        let pass = document.getElementById('pass');
        const submit = document.getElementById('submit');
        const greetings = document.getElementById('div');
        const form = document.getElementById('form');

        function userValidate(){
            let user = new User(login.value, pass.value);
            localStorage.setItem('users', JSON.stringify(Users));
            
            JSON.parse(localUsers).map( (item) => {
                if (user.login === item.login && user.pass === item.pass)
                {
                    user.login.
                    greetings.innerText = "Hello " + user.login;
                    form.style.display = 'none';
                }
            });

            login.value = null;
            pass.value = null;
        }  
        
        submit.addEventListener('click', userValidate);

    })
