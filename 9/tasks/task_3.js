/*
    Задание 3:

    Написать класс Posts в котором есть данные:

    _id
    isActive,
    title,
    about,
    likes,
    created_at

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было прездагрузить данные в данный модуль: http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2

*/
    class Posts{
        constructor(about, _id, created_at, isActive, title){
            this.about = about;
            this._id = _id;
            this.created_at = created_at;
            this.isActive = isActive;
            this.title = title;
            this.likes = 0;
        }

        addLike() {
            this.likes ++;
        }
        render(){
            let node = document.createElement('div');
                node.innerHTML = `
                <h1>${this.about}</h1>
                <h2>${this._id}</h2>
                <h2>${this.created_at}</h2>
                <h2>${this.isActive}</h2>
                <h2>${this.title}</h2>
                <h2>${this.likes}</h2>
                `;

                document.body.appendChild(node);
        }
    }

    let arrayData = [

        {
          "about": "Dolore minim sint pariatur culpa aliquip elit dolore est quis esse ea amet. Ex duis pariatur sit sunt voluptate est labore voluptate id elit irure. Duis nulla amet aliqua laborum magna nulla esse est incididunt velit ut. Exercitation in laborum minim velit occaecat non.\r\n", 
          "_id": "5cb2dd87eba6b1143da8a9d3", 
          "created_at": "2018-03-16T04:56:38 -02:00", 
          "isActive": true, 
          "title": "dolor sit anim id ad"
        }, 
        {
          "about": "Esse quis sit adipisicing mollit sit. Tempor nisi aute cupidatat in voluptate enim nulla. Anim eu cillum incididunt aliquip voluptate sit. Laborum sit sint ea proident incididunt Lorem proident exercitation nulla tempor ex ad duis id. Fugiat amet nulla velit sit veniam officia laborum reprehenderit fugiat mollit exercitation consequat nostrud nostrud. Nulla elit proident aliqua in commodo est enim. Proident tempor ex reprehenderit aliquip id duis laboris irure cillum reprehenderit enim commodo.\r\n", 
          "_id": "5cb2dd87ab26f687dba7c322", 
          "created_at": "2019-01-09T07:44:09 -02:00", 
          "isActive": false, 
          "title": "est aliqua nulla ipsum commodo"
        }, 
        {
          "about": "Laboris officia officia duis aliquip velit enim amet reprehenderit adipisicing enim esse ullamco ad. Sunt nulla nostrud dolor dolor ad aute pariatur dolor sint exercitation excepteur reprehenderit. Laboris veniam qui duis ullamco qui non ex.\r\n", 
          "_id": "5cb2dd874244cf41c8b8f030", 
          "created_at": "2016-02-12T01:20:01 -02:00", 
          "isActive": true, 
          "title": "elit velit qui amet ad"
        }
    ];
    let arrPost = JSON.stringify(arrayData);

    localStorage.setItem('posts', arrPost);
    let posts = JSON.parse(arrPost).map((post) => new Posts(post.about, post._id, post.created_at, post.isActive, post.title));
    // posts.map((post) => post.addLike());
    // posts[0].addLike();
    // posts.map((post) => post.render());
