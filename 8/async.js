/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

  // const showDate = ()

  const showInfo = (infoCompany) =>{
    let table = document.querySelector('table');
    infoCompany.forEach((company, i) => {

      let tr = document.createElement('tr');

      let index = document.createElement('th');
          index.innerHTML = i + 1;

      let companyName = document.createElement('th');
          companyName.innerHTML = company.company;

      let balance = document.createElement('th');
          balance.innerHTML = company.balance;

      let date = document.createElement('th');
          date.innerHTML = `<button>show date</button>`;
          date.querySelectorAll('button').forEach((button) => button.addEventListener('click', function(){
              date.innerHTML = company.registered;
          }));

      let adress = document.createElement('th');
          adress.innerHTML = `<button>show adress</button>`;
          adress.querySelectorAll('button').forEach((button) => button.addEventListener('click', function(){
              adress.innerHTML = `
                  Country: ${company.address.country}<br>
                  City: ${company.address.city}<br>
                  Street: ${company.address.street}<br>
                  House: ${company.address.house}<br>
              `;
          }));
      
      tr.appendChild(index);
      tr.appendChild(companyName);
      tr.appendChild(balance);
      tr.appendChild(date);
      tr.appendChild(adress);
      table.appendChild(tr);
    })
  }
  
  async function getInfoCompany(){
    const companys =  await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
    let infoCompany = await companys.json();
    return await infoCompany;
  }
  
  let showCompany = getInfoCompany();
      showCompany.then(data => showInfo(data));
