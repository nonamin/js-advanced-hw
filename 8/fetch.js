/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/

  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  // fetch(url)
  //   .then(testFunc)
  //   .then(test2Func)
  //   .then( res => {
  //      return fetch()
  //       .then( friendsResponse)
  //       .then()
  //   })
  //   .then( func)

 let ule = document.getElementById('result');
 let div = document.getElementById('person');

  fetch('http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2')
    .then(response => { 
      if( response.status === 200 ){
          return response.json().then( data => { 
            return getRandomIntInclusive(0, data.length);
            // return data[getRandomIntInclusive(0, data.length)]; #вариант без 2-го fetch
          });
      };
    })

    .then (randPerson => {
      return fetch('http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2')
      .then(res  =>
        { 
          return res.json().then( data => {
                return data[randPerson]
                })
        })

      .then( data => { JSON.stringify(data.friends.forEach( item => 
          {
            let li = document.createElement('li');
            li.innerText = item.name;
            ule.appendChild(li);
          })  
      )})
    })
      // JSON.stringify(randPerson.friends.forEach(item =>  { #вариант без 2-го fetch
      //   let li = document.createElement('li');
      //   li.innerText = item.name;
      //   ule.appendChild(li);
      // }));
    // })
    